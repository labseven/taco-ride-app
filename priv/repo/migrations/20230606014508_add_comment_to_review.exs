defmodule TacoRide.Repo.Migrations.AddCommentToReview do
  use Ecto.Migration

  def change do
    alter table(:review) do
      add(:comment, :string)
      add(:photo_url, :string)
      add(:ate_all_tacos, :boolean, default: false)
    end
  end
end
