defmodule TacoRide.Repo.Migrations.UniqueTeamName do
  use Ecto.Migration

  def change do
    alter table(:teams) do
      modify :name, :string, null: false
      add :color, :string, default: "#000000"
    end

    create unique_index(:teams, [:name])
  end
end
