defmodule TacoRide.Repo.Migrations.CreateTacoShops do
  use Ecto.Migration

  def up do
    create table(:taco_shops) do
      add :name, :string, null: false
      add :address, :string
      add :place_id, :string

      timestamps()
    end

    execute("SELECT AddGeometryColumn('taco_shops', 'location', 4326, 'POINT', 2);")
  end

  def down do
    drop table(:taco_shops)
  end
end
