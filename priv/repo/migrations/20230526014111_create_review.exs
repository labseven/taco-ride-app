defmodule TacoRide.Repo.Migrations.CreateReview do
  use Ecto.Migration

  def change do
    create table(:review) do
      add :taco_rating, :integer
      add :bike_rating, :integer
      add :taco_shop_id, references(:taco_shops, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:review, [:taco_shop_id])
    create index(:review, [:user_id])
  end
end
