defmodule TacoRide.VisitsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `TacoRide.Visits` context.
  """

  @doc """
  Generate a review.
  """
  def review_fixture(attrs \\ %{}) do
    {:ok, review} =
      attrs
      |> Enum.into(%{
        bike_rating: 42,
        taco_rating: 42
      })
      |> TacoRide.Visits.create_review()

    review
  end
end
