defmodule TacoRide.PlacesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `TacoRide.Places` context.
  """

  @doc """
  Generate a taco_shop.
  """
  def taco_shop_fixture(attrs \\ %{}) do
    {:ok, taco_shop} =
      attrs
      |> Enum.into(%{
        address: "some address",
        name: "some name",
        place_id: "some place_id"
      })
      |> TacoRide.Places.create_taco_shop()

    taco_shop
  end
end
