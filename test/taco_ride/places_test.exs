defmodule TacoRide.PlacesTest do
  use TacoRide.DataCase

  alias TacoRide.Places

  describe "taco_shops" do
    alias TacoRide.Places.TacoShop

    import TacoRide.PlacesFixtures

    @invalid_attrs %{address: nil, name: nil, place_id: nil}

    test "list_taco_shops/0 returns all taco_shops" do
      taco_shop = taco_shop_fixture()
      assert Places.list_taco_shops() == [taco_shop]
    end

    test "get_taco_shop!/1 returns the taco_shop with given id" do
      taco_shop = taco_shop_fixture()
      assert Places.get_taco_shop!(taco_shop.id) == taco_shop
    end

    test "create_taco_shop/1 with valid data creates a taco_shop" do
      valid_attrs = %{address: "some address", name: "some name", place_id: "some place_id"}

      assert {:ok, %TacoShop{} = taco_shop} = Places.create_taco_shop(valid_attrs)
      assert taco_shop.address == "some address"
      assert taco_shop.name == "some name"
      assert taco_shop.place_id == "some place_id"
    end

    test "create_taco_shop/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Places.create_taco_shop(@invalid_attrs)
    end

    test "update_taco_shop/2 with valid data updates the taco_shop" do
      taco_shop = taco_shop_fixture()
      update_attrs = %{address: "some updated address", name: "some updated name", place_id: "some updated place_id"}

      assert {:ok, %TacoShop{} = taco_shop} = Places.update_taco_shop(taco_shop, update_attrs)
      assert taco_shop.address == "some updated address"
      assert taco_shop.name == "some updated name"
      assert taco_shop.place_id == "some updated place_id"
    end

    test "update_taco_shop/2 with invalid data returns error changeset" do
      taco_shop = taco_shop_fixture()
      assert {:error, %Ecto.Changeset{}} = Places.update_taco_shop(taco_shop, @invalid_attrs)
      assert taco_shop == Places.get_taco_shop!(taco_shop.id)
    end

    test "delete_taco_shop/1 deletes the taco_shop" do
      taco_shop = taco_shop_fixture()
      assert {:ok, %TacoShop{}} = Places.delete_taco_shop(taco_shop)
      assert_raise Ecto.NoResultsError, fn -> Places.get_taco_shop!(taco_shop.id) end
    end

    test "change_taco_shop/1 returns a taco_shop changeset" do
      taco_shop = taco_shop_fixture()
      assert %Ecto.Changeset{} = Places.change_taco_shop(taco_shop)
    end
  end
end
