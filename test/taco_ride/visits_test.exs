defmodule TacoRide.VisitsTest do
  use TacoRide.DataCase

  alias TacoRide.Visits

  describe "review" do
    alias TacoRide.Visits.Review

    import TacoRide.VisitsFixtures

    @invalid_attrs %{bike_rating: nil, taco_rating: nil}

    test "list_review/0 returns all review" do
      review = review_fixture()
      assert Visits.list_review() == [review]
    end

    test "get_review!/1 returns the review with given id" do
      review = review_fixture()
      assert Visits.get_review!(review.id) == review
    end

    test "create_review/1 with valid data creates a review" do
      valid_attrs = %{bike_rating: 42, taco_rating: 42}

      assert {:ok, %Review{} = review} = Visits.create_review(valid_attrs)
      assert review.bike_rating == 42
      assert review.taco_rating == 42
    end

    test "create_review/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Visits.create_review(@invalid_attrs)
    end

    test "update_review/2 with valid data updates the review" do
      review = review_fixture()
      update_attrs = %{bike_rating: 43, taco_rating: 43}

      assert {:ok, %Review{} = review} = Visits.update_review(review, update_attrs)
      assert review.bike_rating == 43
      assert review.taco_rating == 43
    end

    test "update_review/2 with invalid data returns error changeset" do
      review = review_fixture()
      assert {:error, %Ecto.Changeset{}} = Visits.update_review(review, @invalid_attrs)
      assert review == Visits.get_review!(review.id)
    end

    test "delete_review/1 deletes the review" do
      review = review_fixture()
      assert {:ok, %Review{}} = Visits.delete_review(review)
      assert_raise Ecto.NoResultsError, fn -> Visits.get_review!(review.id) end
    end

    test "change_review/1 returns a review changeset" do
      review = review_fixture()
      assert %Ecto.Changeset{} = Visits.change_review(review)
    end
  end
end
