defmodule TacoRideWeb.TacoShopLiveTest do
  use TacoRideWeb.ConnCase

  import Phoenix.LiveViewTest
  import TacoRide.PlacesFixtures

  @create_attrs %{address: "some address", name: "some name", place_id: "some place_id"}
  @update_attrs %{address: "some updated address", name: "some updated name", place_id: "some updated place_id"}
  @invalid_attrs %{address: nil, name: nil, place_id: nil}

  defp create_taco_shop(_) do
    taco_shop = taco_shop_fixture()
    %{taco_shop: taco_shop}
  end

  describe "Index" do
    setup [:create_taco_shop]

    test "lists all taco_shops", %{conn: conn, taco_shop: taco_shop} do
      {:ok, _index_live, html} = live(conn, ~p"/taco_shops")

      assert html =~ "Listing Taco shops"
      assert html =~ taco_shop.address
    end

    test "saves new taco_shop", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/taco_shops")

      assert index_live |> element("a", "New Taco shop") |> render_click() =~
               "New Taco shop"

      assert_patch(index_live, ~p"/taco_shops/new")

      assert index_live
             |> form("#taco_shop-form", taco_shop: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#taco_shop-form", taco_shop: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/taco_shops")

      html = render(index_live)
      assert html =~ "Taco shop created successfully"
      assert html =~ "some address"
    end

    test "updates taco_shop in listing", %{conn: conn, taco_shop: taco_shop} do
      {:ok, index_live, _html} = live(conn, ~p"/taco_shops")

      assert index_live |> element("#taco_shops-#{taco_shop.id} a", "Edit") |> render_click() =~
               "Edit Taco shop"

      assert_patch(index_live, ~p"/taco_shops/#{taco_shop}/edit")

      assert index_live
             |> form("#taco_shop-form", taco_shop: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#taco_shop-form", taco_shop: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/taco_shops")

      html = render(index_live)
      assert html =~ "Taco shop updated successfully"
      assert html =~ "some updated address"
    end

    test "deletes taco_shop in listing", %{conn: conn, taco_shop: taco_shop} do
      {:ok, index_live, _html} = live(conn, ~p"/taco_shops")

      assert index_live |> element("#taco_shops-#{taco_shop.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#taco_shops-#{taco_shop.id}")
    end
  end

  describe "Show" do
    setup [:create_taco_shop]

    test "displays taco_shop", %{conn: conn, taco_shop: taco_shop} do
      {:ok, _show_live, html} = live(conn, ~p"/taco_shops/#{taco_shop}")

      assert html =~ "Show Taco shop"
      assert html =~ taco_shop.address
    end

    test "updates taco_shop within modal", %{conn: conn, taco_shop: taco_shop} do
      {:ok, show_live, _html} = live(conn, ~p"/taco_shops/#{taco_shop}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Taco shop"

      assert_patch(show_live, ~p"/taco_shops/#{taco_shop}/show/edit")

      assert show_live
             |> form("#taco_shop-form", taco_shop: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#taco_shop-form", taco_shop: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/taco_shops/#{taco_shop}")

      html = render(show_live)
      assert html =~ "Taco shop updated successfully"
      assert html =~ "some updated address"
    end
  end
end
