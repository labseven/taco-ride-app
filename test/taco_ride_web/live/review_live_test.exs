defmodule TacoRideWeb.ReviewLiveTest do
  use TacoRideWeb.ConnCase

  import Phoenix.LiveViewTest
  import TacoRide.VisitsFixtures

  @create_attrs %{bike_rating: 42, taco_rating: 42}
  @update_attrs %{bike_rating: 43, taco_rating: 43}
  @invalid_attrs %{bike_rating: nil, taco_rating: nil}

  defp create_review(_) do
    review = review_fixture()
    %{review: review}
  end

  describe "Index" do
    setup [:create_review]

    test "lists all review", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/review")

      assert html =~ "Listing Review"
    end

    test "saves new review", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/review")

      assert index_live |> element("a", "New Review") |> render_click() =~
               "New Review"

      assert_patch(index_live, ~p"/review/new")

      assert index_live
             |> form("#review-form", review: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#review-form", review: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/review")

      html = render(index_live)
      assert html =~ "Review created successfully"
    end

    test "updates review in listing", %{conn: conn, review: review} do
      {:ok, index_live, _html} = live(conn, ~p"/review")

      assert index_live |> element("#review-#{review.id} a", "Edit") |> render_click() =~
               "Edit Review"

      assert_patch(index_live, ~p"/review/#{review}/edit")

      assert index_live
             |> form("#review-form", review: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#review-form", review: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/review")

      html = render(index_live)
      assert html =~ "Review updated successfully"
    end

    test "deletes review in listing", %{conn: conn, review: review} do
      {:ok, index_live, _html} = live(conn, ~p"/review")

      assert index_live |> element("#review-#{review.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#review-#{review.id}")
    end
  end

  describe "Show" do
    setup [:create_review]

    test "displays review", %{conn: conn, review: review} do
      {:ok, _show_live, html} = live(conn, ~p"/review/#{review}")

      assert html =~ "Show Review"
    end

    test "updates review within modal", %{conn: conn, review: review} do
      {:ok, show_live, _html} = live(conn, ~p"/review/#{review}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Review"

      assert_patch(show_live, ~p"/review/#{review}/show/edit")

      assert show_live
             |> form("#review-form", review: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#review-form", review: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/review/#{review}")

      html = render(show_live)
      assert html =~ "Review updated successfully"
    end
  end
end
