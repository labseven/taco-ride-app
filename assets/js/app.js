// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
import socket from "./user_socket";

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html";
// Establish Phoenix Socket and LiveView configuration.
import { Socket } from "phoenix";
import { LiveSocket } from "phoenix_live_view";
import topbar from "../vendor/topbar";

let csrfToken = document
  .querySelector("meta[name='csrf-token']")
  .getAttribute("content");
let liveSocket = new LiveSocket("/live", Socket, {
  params: { _csrf_token: csrfToken },
});

// Show progress bar on live navigation and form submits
topbar.config({ barColors: { 0: "#29d" }, shadowColor: "rgba(0, 0, 0, .3)" });
window.addEventListener("phx:page-loading-start", (_info) => topbar.show(300));
window.addEventListener("phx:page-loading-stop", (_info) => topbar.hide());

// connect if there are any LiveViews on the page
liveSocket.connect();

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket;

// if "taco_map" element exists, build a map
if (window.document.getElementById("taco_map")) {
  const map = L.map("taco_map", { renderer: L.canvas() }).setView(
    [45.53506021185081, -122.60220706778959],
    12
  );

  // map.locate({ watch: true, setView: true, maxZoom: 16 });
  L.control.locate().addTo(map);

  const tiles = L.tileLayer("https://tile.openstreetmap.org/{z}/{x}/{y}.png", {
    maxZoom: 19,
    attribution:
      '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    updateWhenZooming: false,
    updateWhenIdle: true,
  }).addTo(map);

  let channel = socket.channel("taco_shop:all", {});
  channel
    .join()
    .receive("ok", (resp) => {
      console.log("Joined successfully", resp);
    })
    .receive("error", (resp) => {
      console.log("Unable to join", resp);
    });

  function popupHTML(taco_shop) {
    console.log(taco_shop);
    return `<div class="flex flex-col">
    <div>
            <a href="/taco_shops/${taco_shop.id}"><p>${taco_shop.name}</a><br>
            ${taco_shop.address}</p>
            <p class="my-0">${
              taco_shop.reviews.length > 0
                ? taco_shop.reviews.length +
                  " people visited (<a href='/teams/" +
                  taco_shop.reviews[0].team_id +
                  "'>" +
                  taco_shop.reviews[0].team_name +
                  "</a>)"
                : ""
            }${
      taco_shop.reviews.length > 0 && taco_shop.reviews[0].ate_all_tacos
        ? "<br>🌮🌮🌮 They ate every taco on the menu 🌮🌮🌮"
        : ""
    }</p>
            <div class="flex justify-between">
            <a href="/taco_shops/${
              taco_shop.id
            }/show/review"><p>I ate here!</p></a>
            <a href="https://maps.google.com/maps/dir//${
              taco_shop.address
            }"><p>Navigate!</p></a>
            </div>
            </div>
        </div>
            `;
  }
  channel.on("init_shops", (msg) => {
    // console.log(msg);
    shop_markers = {};

    taco_icon = L.icon({
      iconUrl: "/img/taco.png",
      iconSize: [32, 32],
    });

    taco_reserved_icon = L.icon({
      iconUrl: "/img/taco_flag.png",
      iconSize: [54, 44],
    });
    for (taco_shop_id in msg.taco_shops) {
      taco_shop = msg.taco_shops[taco_shop_id];

      // console.log(taco_shop);
      shop_markers[taco_shop_id] = L.marker(
        taco_shop.location.coordinates.reverse(),
        {
          icon:
            taco_shop.reviews.length > 0
              ? taco_reviewed_icon(taco_shop.reviews[0])
              : taco_icon,
        }
      )
        .bindPopup(popupHTML(taco_shop))
        .addTo(map);
    }
  });

  channel.on("taco_shop_captured", (msg) => {
    shop_markers[msg.taco_shop_id].setIcon(taco_reviewed_icon(msg));
  });
}

function taco_reviewed_icon(review) {
  if (review.team_color[0] == "#") {
    review.team_color = review.team_color.slice(1, review.team_color.length);
  }

  flag_url = review.ate_all_tacos
    ? "/flag_star.svg?color="
    : "/flag.svg?color=";

  icon_size = review.ate_all_tacos ? [44, 44] : [32, 32];
  return L.icon({
    iconUrl: flag_url + review.team_color,
    iconSize: icon_size,
  });
}

function taco_reviewed_all_eaten_icon(team_color) {
  if (team_color[0] == "#") {
    team_color = team_color.slice(1, team_color.length);
  }
  return L.icon({
    iconUrl: "/flag_star.svg?color=" + team_color,
    iconSize: [32, 32],
  });
}
