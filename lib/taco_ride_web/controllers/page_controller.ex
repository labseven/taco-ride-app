defmodule TacoRideWeb.PageController do
  use TacoRideWeb, :controller

  def home(conn, _params) do
    # The home page is often custom made,
    # so skip the default app layout.
    render(conn, :home, layout: false)
  end

  def about(conn, _params) do
    render(conn, "about.html")
  end

  def team_flag(conn, params) do
    color =
      if params["color"] do
        params["color"]
      else
        "800080"
      end

    conn = put_resp_content_type(conn, "image/svg+xml")

    send_resp(
      conn,
      200,
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   width=\"1000\"
   height=\"1000\"
   viewBox=\"0 0 264.58332 264.58334\"
   version=\"1.1\"
   id=\"svg5\"
   xmlns=\"http://www.w3.org/2000/svg\"
   xmlns:svg=\"http://www.w3.org/2000/svg\"
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
   xmlns:cc=\"http://creativecommons.org/ns#\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\">
  <defs
     id=\"defs2\" />
  <metadata
     id=\"metadata856\">
    <rdf:RDF>
      <cc:Work
         rdf:about=\"\">
        <dc:creator>
          <cc:Agent>
            <dc:title>Inspire Studio</dc:title>
          </cc:Agent>
        </dc:creator>
        <cc:license
           rdf:resource=\"http://creativecommons.org/publicdomain/zero/1.0/\" />
        <dc:description>Thank you for using this artwork. Feel free to visit our page https://www.pinterest.com.au/inspirestudiobox for more designs. We wish you have a wonderful day, everyday. Amitabha</dc:description>
      </cc:Work>
      <cc:License
         rdf:about=\"http://creativecommons.org/publicdomain/zero/1.0/\">
        <cc:permits
           rdf:resource=\"http://creativecommons.org/ns#Reproduction\" />
        <cc:permits
           rdf:resource=\"http://creativecommons.org/ns#Distribution\" />
        <cc:permits
           rdf:resource=\"http://creativecommons.org/ns#DerivativeWorks\" />
      </cc:License>
    </rdf:RDF>
  </metadata>
  <g
     id=\"g4936\"
     transform=\"matrix(1.8687897,0,0,1.8690365,17.86717,-584.8807)\"
     style=\"stroke-width:1.00407\">
    <g
       id=\"g1037\"
       transform=\"translate(5.3135636)\">
      <path
         style=\"color:#000000;fill:#000000;fill-opacity:0;stroke-width:7.9698;stroke-linecap:round;-inkscape-stroke:none\"
         d=\"M 5.6920791,324.46734 V 446.15385\"
         id=\"path3838\" />
      <path
         id=\"path3840\"
         style=\"color:#000000;fill:#4d4d4d;stroke-width:3.79491;stroke-linecap:round;-inkscape-stroke:none\"
         d=\"m 6.5117188,1252.9805 v 433.2715 a 15,15 0 0 0 15.0000002,15 15,15 0 0 0 15,-15 v -433.2696 a 32.499999,32.50129 0 0 1 -3.882813,1.8106 32.499999,32.50129 0 0 1 -5.472656,1.4648 32.499999,32.50129 0 0 1 -5.642578,0.4942 32.499999,32.50129 0 0 1 -5.644531,-0.4942 32.499999,32.50129 0 0 1 -5.470703,-1.4648 32.499999,32.50129 0 0 1 -3.8867192,-1.8125 z\"
         transform=\"scale(0.26458333)\" />
      <path
         style=\"fill:#" <>
        color <>
        ";stroke:#4d4d4d;stroke-width:7.9698;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"
         d=\"m 17.351045,333.99583 v 47.52332 c 49.05637,-29.42359 45.037024,25.8921 91.417565,0 v -47.52332 c -46.380541,25.89211 -42.361195,-29.42359 -91.417565,0 z\"
         id=\"path3802\" />
      <ellipse
         style=\"fill:#4d4d4d;fill-opacity:1;stroke:none;stroke-width:7.9698;paint-order:fill markers stroke\"
         id=\"ellipse3804\"
         cx=\"5.6920772\"
         cy=\"323.91635\"
         rx=\"6.6143208\"
         ry=\"6.614583\" />
    </g>
  </g>
</svg>
"
    )
  end

  def team_flag_star(conn, params) do
    color =
      if params["color"] do
        params["color"]
      else
        "800080"
      end

    conn = put_resp_content_type(conn, "image/svg+xml")

    send_resp(
      conn,
      200,
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   width=\"1007.3062\"
   height=\"864.0943\"
   viewBox=\"0 0 266.51641 228.62496\"
   version=\"1.1\"
   id=\"svg5\"
   xmlns=\"http://www.w3.org/2000/svg\"
   xmlns:svg=\"http://www.w3.org/2000/svg\"
   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
   xmlns:cc=\"http://creativecommons.org/ns#\"
   xmlns:dc=\"http://purl.org/dc/elements/1.1/\">
  <defs
     id=\"defs2\" />
  <metadata
     id=\"metadata856\">
    <rdf:RDF>
      <cc:Work
         rdf:about=\"\">
        <dc:creator>
          <cc:Agent>
            <dc:title>Inspire Studio</dc:title>
          </cc:Agent>
        </dc:creator>
        <cc:license
           rdf:resource=\"http://creativecommons.org/publicdomain/zero/1.0/\" />
        <dc:description>Thank you for using this artwork. Feel free to visit our page https://www.pinterest.com.au/inspirestudiobox for more designs. We wish you have a wonderful day, everyday. Amitabha</dc:description>
      </cc:Work>
      <cc:License
         rdf:about=\"http://creativecommons.org/publicdomain/zero/1.0/\">
        <cc:permits
           rdf:resource=\"http://creativecommons.org/ns#Reproduction\" />
        <cc:permits
           rdf:resource=\"http://creativecommons.org/ns#Distribution\" />
        <cc:permits
           rdf:resource=\"http://creativecommons.org/ns#DerivativeWorks\" />
      </cc:License>
    </rdf:RDF>
  </metadata>
  <g
     id=\"g4936\"
     transform=\"matrix(1.8687897,0,0,1.8690365,11.097017,-544.44634)\"
     style=\"stroke-width:1.00407\">
    <g
       id=\"g1037\"
       transform=\"translate(5.3135636)\">
      <path
         style=\"color:#000000;fill:#000000;fill-opacity:0;stroke-width:7.9698;stroke-linecap:round;-inkscape-stroke:none\"
         d=\"M 5.6920791,324.46734 V 446.15385\"
         id=\"path3838\" />
      <path
         id=\"path3840\"
         style=\"color:#000000;fill:#4d4d4d;stroke-width:3.79491;stroke-linecap:round;-inkscape-stroke:none\"
         d=\"m 6.5117188,1252.9805 v 433.2715 a 15,15 0 0 0 15.0000002,15 15,15 0 0 0 15,-15 v -433.2696 a 32.499999,32.50129 0 0 1 -3.882813,1.8106 32.499999,32.50129 0 0 1 -5.472656,1.4648 32.499999,32.50129 0 0 1 -5.642578,0.4942 32.499999,32.50129 0 0 1 -5.644531,-0.4942 32.499999,32.50129 0 0 1 -5.470703,-1.4648 32.499999,32.50129 0 0 1 -3.8867192,-1.8125 z\"
         transform=\"scale(0.26458333)\" />
      <path
         style=\"fill:#" <>
        color <>
        ";stroke:#4d4d4d;stroke-width:7.9698;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\"
         d=\"m 17.351045,333.99583 v 47.52332 c 49.05637,-29.42359 45.037024,25.8921 91.417565,0 v -47.52332 c -46.380541,25.89211 -42.361195,-29.42359 -91.417565,0 z\"
         id=\"path3802\" />
      <ellipse
         style=\"fill:#4d4d4d;fill-opacity:1;stroke:none;stroke-width:7.9698;paint-order:fill markers stroke\"
         id=\"ellipse3804\"
         cx=\"5.6920772\"
         cy=\"323.91635\"
         rx=\"6.6143208\"
         ry=\"6.614583\" />
      <path
         style=\"fill:#edb600;fill-opacity:1;stroke:#000000;stroke-width:15;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\"
         id=\"path885\"
         d=\"M 185.66517,65.369876 90.624349,65.244766 36.641197,143.46619 7.3909545,53.038339 -83.683754,25.869061 -6.7205768,-29.893317 -9.0246905,-124.90628 67.791412,-68.941472 157.4421,-100.49343 127.95388,-10.142903 Z\"
         transform=\"matrix(0.14158005,0,0,0.14156135,39.986309,390.93735)\" />
      <path
         style=\"fill:#edb600;fill-opacity:1;stroke:#000000;stroke-width:15;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\"
         id=\"path4129\"
         d=\"m 801.60803,331.07832 -64.25619,-74.30227 -97.63627,10.80962 50.80941,-84.07194 -40.45183,-89.517251 95.65814,22.342961 72.63566,-66.134327 8.31057,97.880647 85.34314,48.64399 -90.52192,38.15061 z\"
         transform=\"matrix(0.14158005,0,0,0.14156135,-0.15616862,326.85775)\" />
      <path
         style=\"fill:#edb600;fill-opacity:1;stroke:#000000;stroke-width:15;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\"
         id=\"path4131\"
         d=\"m 476.6085,755.27626 -91.00328,-81.56973 -118.78636,28.7232 49.45587,-111.7557 -64.0244,-104.09658 121.56869,12.50091 79.2171,-93.05843 25.67772,119.48169 112.98326,46.58331 -105.69899,61.34283 z\"
         transform=\"matrix(0.14158005,0,0,0.14156135,-44.933614,236.14745)\" />
      <path
         style=\"fill:#edb600;fill-opacity:1;stroke:#000000;stroke-width:15;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\"
         id=\"path4133\"
         d=\"m 795.65442,741.64019 -63.61601,-19.58065 -52.19497,41.30478 -1.03612,-66.55318 -55.41231,-36.87648 62.97565,-21.55148 17.94828,-64.0957 39.95722,53.23364 66.50495,-2.73684 -38.28074,54.45167 z\"
         transform=\"matrix(0.14158005,0,0,0.14156135,-43.376583,228.6647)\" />
    </g>
  </g>
</svg>
"
    )
  end
end
