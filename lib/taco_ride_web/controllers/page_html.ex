defmodule TacoRideWeb.PageHTML do
  use TacoRideWeb, :html

  embed_templates "page_html/*"
end
