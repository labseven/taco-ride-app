defmodule TacoRideWeb.TacoShopChannel do
  use TacoRideWeb, :channel

  alias TacoRide.Places

  @impl true
  def join("taco_shop:all", _payload, socket) do
    send(self(), :after_join)
    {:ok, socket}
  end

  @impl true
  def handle_info(:after_join, socket) do
    taco_shops = Places.list_taco_shops()

    locations =
      for taco_shop <- taco_shops,
          into: %{},
          do:
            {taco_shop.id,
             %{
               id: taco_shop.id,
               name: taco_shop.name,
               address: taco_shop.address,
               reviews:
                 taco_shop.reviews
                 |> Enum.sort_by(& &1.inserted_at)
                 |> Enum.map(fn review ->
                   %{
                     bike_rating: review.bike_rating,
                     taco_rating: review.taco_rating,
                     team_color: review.user.team.color,
                     team_name: review.user.team.name,
                     team_id: review.user.team.id,
                     ate_all_tacos: review.ate_all_tacos
                   }
                 end),
               location:
                 taco_shop.location
                 |> Geo.JSON.encode!()
             }}

    # taco_shops
    # |> Enum.map(fn taco_shop ->
    #   %{
    #     id: taco_shop.id,
    #     name: taco_shop.name,
    #     location:
    #       taco_shop.location
    #       |> Geo.JSON.encode!()
    #   }
    # end)

    push(socket, "init_shops", %{taco_shops: locations})
    {:noreply, socket}
  end

  @impl true
  def handle_in("reserve_taco_shop", payload, socket) do
    broadcast(socket, "reserve_taco_shop", payload)
    {:noreply, socket}
  end
end
