defmodule TacoRideWeb.Router do
  alias PageController
  use TacoRideWeb, :router

  import TacoRideWeb.UserAuth

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_live_flash)
    plug(:put_root_layout, {TacoRideWeb.Layouts, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(:fetch_current_user)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", TacoRideWeb do
    pipe_through(:browser)

    live_session(:taco_view,
      on_mount: [
        {TacoRideWeb.UserAuth, :mount_current_user}
      ]
    ) do
      live("/", TacoShopLive.Index, :index)
      live("/taco_shops", TacoShopLive.Index, :index)
      live("/taco_shops/new", TacoShopLive.Index, :new)
      live("/taco_shops/:id/edit", TacoShopLive.Index, :edit)

      live("/taco_shops/:id", TacoShopLive.Show, :show)
      live("/taco_shops/:id/show/edit", TacoShopLive.Show, :edit)

      live("/teams", TeamLive.Index, :index)
      live("/teams/:id", TeamLive.Show, :show)

      live("/bikers/:id", UserLive.Show, :show)
    end

    get("/about", PageController, :about)
    get("/flag.svg", PageController, :team_flag)
    get("/flag_star.svg", PageController, :team_flag_star)

    live_session(:taco_review,
      on_mount: [
        {TacoRideWeb.UserAuth, :ensure_authenticated}
      ]
    ) do
      live("/taco_shops/:id/show/review", TacoShopLive.Show, :review)
    end
  end

  # Other scopes may use custom stacks.
  # scope "/api", TacoRideWeb do
  #   pipe_through :api
  # end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:taco_ride, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through(:browser)

      live_dashboard("/dashboard", metrics: TacoRideWeb.Telemetry)
      forward("/mailbox", Plug.Swoosh.MailboxPreview)
    end
  end

  ## Authentication routes

  scope "/", TacoRideWeb do
    pipe_through([:browser, :redirect_if_user_is_authenticated])

    live_session :redirect_if_user_is_authenticated,
      on_mount: [{TacoRideWeb.UserAuth, :redirect_if_user_is_authenticated}] do
      live("/users/register", UserRegistrationLive, :new)
      live("/users/log_in", UserLoginLive, :new)
      live("/users/log_in/new_team", UserLoginLive, :new_team)
      live("/users/reset_password", UserForgotPasswordLive, :new)
      live("/users/reset_password/:token", UserResetPasswordLive, :edit)
    end

    post("/users/log_in", UserSessionController, :create)
  end

  # scope "/", TacoRideWeb do
  #   pipe_through [:browser, :require_authenticated_user]

  #   # live_session :require_authenticated_user,
  #   #   on_mount: [{TacoRideWeb.UserAuth, :ensure_authenticated}] do
  #   #   live "/users/settings", UserSettingsLive, :edit
  #   #   live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email
  #   # end
  # end

  scope "/", TacoRideWeb do
    pipe_through([:browser])

    delete("/users/log_out", UserSessionController, :delete)

    live_session :current_user,
      on_mount: [{TacoRideWeb.UserAuth, :mount_current_user}] do
      live("/users/confirm/:token", UserConfirmationLive, :edit)
      live("/users/confirm", UserConfirmationInstructionsLive, :new)
    end
  end
end
