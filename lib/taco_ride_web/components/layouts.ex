defmodule TacoRideWeb.Layouts do
  use TacoRideWeb, :html

  embed_templates "layouts/*"
end
