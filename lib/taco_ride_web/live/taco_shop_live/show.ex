defmodule TacoRideWeb.TacoShopLive.Show do
  use TacoRideWeb, :live_view

  alias TacoRide.Places

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:taco_shop, Places.get_taco_shop!(id))}
  end

  defp page_title(:show), do: "Show Taco shop"
  defp page_title(:edit), do: "Edit Taco shop"
  defp page_title(:review), do: "Review Taco shop"
end
