defmodule TacoRideWeb.TacoShopLive.FormComponent do
  use TacoRideWeb, :live_component

  alias TacoRide.Places

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage taco_shop records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="taco_shop-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:address]} type="text" label="Address" />
        <.input field={@form[:place_id]} type="text" label="Place" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Taco shop</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{taco_shop: taco_shop} = assigns, socket) do
    changeset = Places.change_taco_shop(taco_shop)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"taco_shop" => taco_shop_params}, socket) do
    changeset =
      socket.assigns.taco_shop
      |> Places.change_taco_shop(taco_shop_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"taco_shop" => taco_shop_params}, socket) do
    save_taco_shop(socket, socket.assigns.action, taco_shop_params)
  end

  defp save_taco_shop(socket, :edit, taco_shop_params) do
    case Places.update_taco_shop(socket.assigns.taco_shop, taco_shop_params) do
      {:ok, taco_shop} ->
        notify_parent({:saved, taco_shop})

        {:noreply,
         socket
         |> put_flash(:info, "Taco shop updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_taco_shop(socket, :new, taco_shop_params) do
    case Places.create_taco_shop(taco_shop_params) do
      {:ok, taco_shop} ->
        notify_parent({:saved, taco_shop})

        {:noreply,
         socket
         |> put_flash(:info, "Taco shop created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
