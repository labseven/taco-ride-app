defmodule TacoRideWeb.TacoShopLive.Index do
  use TacoRideWeb, :live_view

  alias TacoRide.Places
  alias TacoRide.Places.TacoShop

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :taco_shops, Places.list_taco_shops()),
     layout: {TacoRideWeb.Layouts, :map}}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Taco shop")
    |> assign(:taco_shop, Places.get_taco_shop!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Taco shop")
    |> assign(:taco_shop, %TacoShop{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Taco shops")
    |> assign(:taco_shop, nil)
  end

  @impl true
  def handle_info({TacoRideWeb.TacoShopLive.FormComponent, {:saved, taco_shop}}, socket) do
    {:noreply, stream_insert(socket, :taco_shops, taco_shop)}
  end

  # @impl true
  # def handle_event("delete", %{"id" => id}, socket) do
  #   taco_shop = Places.get_taco_shop!(id)
  #   {:ok, _} = Places.delete_taco_shop(taco_shop)

  #   {:noreply, stream_delete(socket, :taco_shops, taco_shop)}
  # end
end
