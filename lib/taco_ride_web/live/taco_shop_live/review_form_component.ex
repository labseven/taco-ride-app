defmodule TacoRideWeb.TacoShopLive.ReviewFormComponent do
  use TacoRideWeb, :live_component

  alias TacoRide.Places
  alias TacoRide.Visits
  alias TacoRide.Accounts

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle><%= @taco_shop.name %></:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="taco_shop-review-form"
        phx-target={@myself}
        phx-change="validate-review"
        phx-submit="review"
      >
        <.input
          field={@form[:taco_rating]}
          type="select"
          options={[1, 2, 3, 4, 5]}
          label="Taco rating"
        />
        <.input
          field={@form[:bike_rating]}
          type="select"
          options={[1, 2, 3, 4, 5]}
          label="Bike rating"
        />

        <.input
          field={@form[:ate_all_tacos]}
          type="checkbox"
          label="My team ate every taco on the menu!"
        />

        <.input
          field={@form[:comment]}
          type="textarea"
          label="Any comments?"
        />

        <section phx-drop-target={@uploads.photo.ref}>
        <h2 class="block text-sm font-semibold leading-6 text-zinc-800">Picture of your tacos</h2>
        <.live_file_input upload={@uploads.photo} />
          <article class="upload-entry">
          <%= for entry <- @uploads.photo.entries do %>
            <figure>
              <.live_img_preview entry={entry} />
            </figure>
            <progress value={entry.progress} max="100" class="w-full"> <%= entry.progress %>% </progress>
          <% end %>
          </article>
          <%= if @form.errors != [] and @form.source.action == :insert do %>
            <p class="text-red-500">No photo selected</p>
          <% end %>
        </section>
        <:actions>
          <.button phx-disable-with="Saving...">Review Taco shop</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{taco_shop: taco_shop} = assigns, socket) do
    changeset = Places.change_taco_shop(taco_shop)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:uploaded_photo, nil)
     |> allow_upload(:photo, accept: ["image/*"], max_entries: 1, auto_upload: true)
     |> assign_form(changeset)}
  end

  def handle_event("validate-review", %{"review" => review_params}, socket),
    do: handle_event("validate-review", %{"taco_shop" => review_params}, socket)

  @impl true
  def handle_event("validate-review", %{"taco_shop" => review_params}, socket) do
    changeset =
      %TacoRide.Visits.Review{}
      |> Visits.change_review(review_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  @impl true
  def handle_event("validate", %{"taco_shop" => taco_shop_params}, socket) do
    changeset =
      socket.assigns.taco_shop
      |> Places.change_taco_shop(taco_shop_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("review", %{"review" => review_params}, socket),
    do: handle_event("review", %{"taco_shop" => review_params}, socket)

  def handle_event("review", %{"taco_shop" => review_params}, socket) do
    review_params = Map.put(review_params, "taco_shop_id", socket.assigns.taco_shop.id)
    review_params = Map.put(review_params, "user_id", socket.assigns.current_user.id)

    file_paths =
      consume_uploaded_entries(socket, :photo, fn %{path: path}, entry ->
        file_location = [
          "uploads",
          "review_photos",
          "#{DateTime.now!("Etc/UTC") |> Calendar.strftime("%y%m%d%H%M%S")}_taco_shop_#{socket.assigns.taco_shop.id}_user_#{socket.assigns.current_user.id}#{Path.extname(entry.client_name)}"
        ]

        dest = Path.join([:code.priv_dir(:taco_ride), "static"] ++ file_location)
        # The `static/uploads` directory must exist for `File.cp!/2`
        # and MyAppWeb.static_paths/0 should contain uploads to work,.
        File.cp!(path, dest)

        {:ok, Path.join(["/"] ++ file_location)}
      end)

    photo_url =
      if length(file_paths) == 0 do
        nil
      else
        [photo_url | _] = file_paths
        photo_url
      end

    review_params = Map.put(review_params, "photo_url", photo_url)

    review_taco_shop(socket, socket.assigns.action, review_params)
  end

  defp review_taco_shop(socket, :edit, taco_shop_params) do
    case Places.update_taco_shop(socket.assigns.taco_shop, taco_shop_params) do
      {:ok, taco_shop} ->
        notify_parent({:saved, taco_shop})

        {:noreply,
         socket
         |> put_flash(:info, "Taco shop updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp review_taco_shop(socket, :review, taco_shop_params) do
    case Visits.create_review(taco_shop_params) do
      {:ok, review} ->
        # notify_parent({:saved, taco_shop})
        # IO.inspect(review, label: "review")

        [first_review | _shop_reviews] = Visits.list_reviews_for_shop(review.taco_shop_id)
        # IO.inspect(first_review, label: "first_review")

        if first_review.id == review.id do
          user = Accounts.get_user!(review.user_id)

          TacoRideWeb.Endpoint.broadcast("taco_shop:all", "taco_shop_captured", %{
            taco_shop_id: review.taco_shop_id,
            team_color: user.team.color,
            ate_all_tacos: review.ate_all_tacos
          })
        end

        {:noreply,
         socket
         |> put_flash(:info, "Review created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
