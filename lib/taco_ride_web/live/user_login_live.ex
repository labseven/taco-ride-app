defmodule TacoRideWeb.UserLoginLive do
  alias TacoRide.Accounts
  alias TacoRide.Accounts.Team

  use TacoRideWeb, :live_view

  def render(assigns) do
    ~H"""
    <div class="mx-auto max-w-sm">
      <.header class="text-center">
        Sign in or Register your account
      </.header>

      <.simple_form for={@form} id="login_form" action={~p"/users/log_in"} phx-update="ignore">
        <.input field={@form[:username]} type="text" label="username" required />
        <.input
          field={@form[:team_id]}
          type="select"
          options={@streams.teams |> Enum.map(fn {_, team} -> {team.name, team.id} end)}
          label="Team"
          required
        />

        <:actions>
          <.link href={~p"/users/log_in/new_team"} class="text-sm font-semibold">
            Register New Team
          </.link>
        </:actions>
        <:actions>
          <.button phx-disable-with="Signing in..." class="w-full">
            Sign in <span aria-hidden="true">→</span>
          </.button>
        </:actions>
      </.simple_form>
    </div>

    <.modal
      :if={@live_action in [:new_team]}
      id="team-modal"
      show
      on_cancel={JS.patch(~p"/users/log_in")}
    >
      <.live_component
        module={TacoRideWeb.TeamLive.FormComponent}
        id={@team.id || :new}
        title={@page_title}
        action={@live_action}
        team={@team}
        patch={~p"/users/log_in"}
      />
    </.modal>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    username = live_flash(socket.assigns.flash, :team)
    socket = stream(socket, :teams, Accounts.list_teams())
    form = to_form(%{"username" => username}, as: "user")
    {:ok, assign(socket, form: form), temporary_assigns: [form: form]}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :new_team, _params) do
    socket
    |> assign(:login_socket, socket)
    |> assign(:page_title, "New Team")
    |> assign(:team, %Team{})
  end

  defp apply_action(socket, :new, _params) do
    socket
  end

  @impl true
  def handle_info({TacoRideWeb.TeamLive.FormComponent, {:saved, team}}, socket) do
    # need to push_navigate to get new team to show up. Cannot access parent socket to stream_insert into
    {:noreply, push_navigate(socket, to: ~p"/users/log_in", flash: [team: team.name])}
  end
end
