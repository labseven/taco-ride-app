defmodule TacoRideWeb.ReviewLive.Index do
  use TacoRideWeb, :live_view

  alias TacoRide.Visits
  alias TacoRide.Visits.Review

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :review_collection, Visits.list_review())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Review")
    |> assign(:review, Visits.get_review!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Review")
    |> assign(:review, %Review{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Review")
    |> assign(:review, nil)
  end

  @impl true
  def handle_info({TacoRideWeb.ReviewLive.FormComponent, {:saved, review}}, socket) do
    {:noreply, stream_insert(socket, :review_collection, review)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    review = Visits.get_review!(id)
    {:ok, _} = Visits.delete_review(review)

    {:noreply, stream_delete(socket, :review_collection, review)}
  end
end
