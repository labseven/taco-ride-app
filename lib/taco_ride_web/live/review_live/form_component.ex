defmodule TacoRideWeb.ReviewLive.FormComponent do
  use TacoRideWeb, :live_component

  alias TacoRide.Visits
  alias TacoRide.Accounts

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage review records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="review-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:taco_rating]} type="number" label="Taco rating" />
        <.input field={@form[:bike_rating]} type="number" label="Bike rating" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Review</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{review: review} = assigns, socket) do
    changeset = Visits.change_review(review)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"review" => review_params}, socket) do
    changeset =
      socket.assigns.review
      |> Visits.change_review(review_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"review" => review_params}, socket) do
    save_review(socket, socket.assigns.action, review_params)
  end

  defp save_review(socket, :edit, review_params) do
    case Visits.update_review(socket.assigns.review, review_params) do
      {:ok, review} ->
        notify_parent({:saved, review})

        {:noreply,
         socket
         |> put_flash(:info, "Review updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_review(socket, :new, review_params) do
    case Visits.create_review(review_params) do
      {:ok, review} ->
        # notify_parent({:saved, review})
        # IO.inspect(review, label: "review")

        [first_review | _shop_reviews] = Visits.list_reviews_for_shop(review.taco_shop_id)
        # IO.inspect(first_review, label: "first_review")

        if first_review.id == review.id do
          user = Accounts.get_user!(review.user_id)

          TacoRideWeb.Endpoint.broadcast("taco_shop:all", "taco_shop_captured", %{
            taco_shop_id: review.taco_shop_id,
            team: user.team,
            ate_all_tacos: review.ate_all_tacos
          })
        end

        {:noreply,
         socket
         |> put_flash(:info, "Review created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
