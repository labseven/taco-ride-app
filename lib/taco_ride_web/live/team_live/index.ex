defmodule TacoRideWeb.TeamLive.Index do
  use TacoRideWeb, :live_view

  alias TacoRide.Accounts
  alias TacoRide.Accounts.Team

  @impl true
  def mount(_params, _session, socket) do
    teams =
      Accounts.list_teams()
      |> Enum.map(fn team ->
        team
        |> Map.put(
          :points,
          Accounts.get_team_points(team)
        )
        |> Map.put(
          :reviews,
          team.users |> Enum.flat_map(fn user -> user.reviews end)
        )
      end)
      |> Enum.sort_by(fn team -> team.points.total end, :desc)

    {:ok, stream(socket, :teams, teams)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Team")
    |> assign(:team, Accounts.get_team!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Team")
    |> assign(:team, %Team{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Teams")
    |> assign(:team, nil)
  end

  @impl true
  def handle_info({TacoRideWeb.TeamLive.FormComponent, {:saved, team}}, socket) do
    {:noreply, stream_insert(socket, :teams, team)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    team = Accounts.get_team!(id)
    {:ok, _} = Accounts.delete_team(team)

    {:noreply, stream_delete(socket, :teams, team)}
  end
end
