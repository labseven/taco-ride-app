defmodule TacoRideWeb.TeamLive.Show do
  use TacoRideWeb, :live_view

  alias TacoRide.Accounts

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    team = id |> Accounts.get_team!()
    team = Map.put(team, :points, Accounts.get_team_points(team))

    reviews = team.users |> Enum.map(fn user -> user.reviews end) |> List.flatten()

    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:team, team)
     |> assign(:reviews, reviews)}
  end

  defp page_title(:show), do: "Show Team"
  defp page_title(:edit), do: "Edit Team"
end
