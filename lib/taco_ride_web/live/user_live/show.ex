defmodule TacoRideWeb.UserLive.Show do
  use TacoRideWeb, :live_view

  alias TacoRide.Accounts

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    user = id |> Accounts.get_user!()

    {:noreply,
     socket
     |> assign(:page_title, "User: #{user.username}")
     |> assign(:user, user)}
  end
end
