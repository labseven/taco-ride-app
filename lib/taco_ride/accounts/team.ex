defmodule TacoRide.Accounts.Team do
  use Ecto.Schema
  import Ecto.Changeset

  schema "teams" do
    field :contact, :string
    field :name, :string

    field :color, :string, default: "#000000"

    has_many :users, TacoRide.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:name, :contact, :color])
    |> validate_required([:name, :contact])
    |> unique_constraint(:name)
  end
end
