defmodule TacoRide.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :username

    has_many :reviews, TacoRide.Visits.Review
    belongs_to :team, TacoRide.Accounts.Team

    timestamps()
  end

  @doc """
  A user changeset for registration.
  """
  def registration_changeset(user, attrs, _opts \\ []) do
    user
    |> cast(attrs, [:username, :team_id])
    |> validate_required([:username, :team_id])
    |> unique_constraint(:username)
    |> assoc_constraint(:team)
  end
end
