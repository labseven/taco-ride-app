defmodule TacoRide.Repo do
  use Ecto.Repo,
    otp_app: :taco_ride,
    adapter: Ecto.Adapters.Postgres,
    types: TacoRide.PostgresTypes
end
