defmodule TacoRide.Accounts do
  @moduledoc """
  The Accounts context.
  """

  import Ecto.Query, warn: false
  alias TacoRide.Repo

  alias TacoRide.Accounts.{User, UserToken}
  alias TacoRide.Visits.Review

  ## Database getters

  def get_user_by_username_team(username, team_id) do
    query =
      from(u in User,
        where: u.username == ^username,
        where: u.team_id == ^team_id,
        select: u
      )

    Repo.one(query)
    |> Repo.preload(:team)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id) |> Repo.preload([:team, reviews: [:taco_shop]])

  ## User registration

  @doc """
  Registers a user.

  ## Examples

      iex> register_user(%{field: value})
      {:ok, %User{}}

      iex> register_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def register_user(attrs) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  ## Session

  @doc """
  Generates a session token.
  """
  def generate_user_session_token(user) do
    {token, user_token} = UserToken.build_session_token(user)
    Repo.insert!(user_token)
    token
  end

  @doc """
  Gets the user with the given signed token.
  """
  def get_user_by_session_token(token) do
    {:ok, query} = UserToken.verify_session_token_query(token)
    Repo.one(query) |> Repo.preload(:team)
  end

  @doc """
  Deletes the signed token with the given context.
  """
  def delete_user_session_token(token) do
    Repo.delete_all(UserToken.token_and_context_query(token, "session"))
    :ok
  end

  alias TacoRide.Accounts.Team

  @doc """
  Returns the list of teams.

  ## Examples

      iex> list_teams()
      [%Team{}, ...]

  """
  def list_teams do
    Repo.all(Team) |> Repo.preload(users: [reviews: [:user, :taco_shop]])
  end

  @doc """
  Gets a single team.

  Raises `Ecto.NoResultsError` if the Team does not exist.

  ## Examples

      iex> get_team!(123)
      %Team{}

      iex> get_team!(456)
      ** (Ecto.NoResultsError)

  """
  def get_team!(id),
    do: Repo.get!(Team, id) |> Repo.preload(users: [reviews: [:user, :taco_shop]])

  @doc """
  Creates a team.

  ## Examples

      iex> create_team(%{field: value})
      {:ok, %Team{}}

      iex> create_team(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_team(attrs \\ %{}) do
    %Team{}
    |> Team.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a team.

  ## Examples

      iex> update_team(team, %{field: new_value})
      {:ok, %Team{}}

      iex> update_team(team, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_team(%Team{} = team, attrs) do
    team
    |> Team.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a team.

  ## Examples

      iex> delete_team(team)
      {:ok, %Team{}}

      iex> delete_team(team)
      {:error, %Ecto.Changeset{}}

  """
  def delete_team(%Team{} = team) do
    Repo.delete(team)
  end

  def get_user_points(user) do
    query =
      from(r in "review",
        join: user in TacoRide.Accounts.User,
        on: user.id == r.user_id,
        select: %{
          user_id: r.user_id,
          taco_shop_id: r.taco_shop_id,
          position:
            fragment(
              "row_number() OVER (PARTITION BY ? ORDER BY ?)",
              r.taco_shop_id,
              r.inserted_at
            ),
          team_position:
            fragment(
              "row_number() OVER (PARTITION BY ?,? ORDER BY ?)",
              r.taco_shop_id,
              user.team_id,
              r.inserted_at
            )
        }
      )

    query2 =
      from(user in subquery(query),
        where: user.user_id == ^user.id,
        select: user
      )

    reviews = Repo.all(query2)

    all_points =
      reviews
      |> Enum.map(fn review ->
        case review do
          # 5 points for first review, 3 points for first review for each team, 1 point for each review after that
          %{position: 1} -> 5
          %{team_position: 1} -> 3
          _ -> 1
        end
      end)
      |> Enum.sum()

    # count number of unique shops
    # total_shops = reviews |> Enum.map(fn review -> review.taco_shop_id end) |> Enum.uniq()
    all_points
  end

  def get_team_points(team) do
    user_points =
      from(u in User, where: u.team_id == ^team.id, select: u)
      |> Repo.all()
      |> Enum.map(fn user -> user |> Map.put(:points, get_user_points(user)) end)

    total_points =
      user_points
      |> Enum.map(fn user -> user.points end)
      |> Enum.sum()

    %{total: total_points, users: user_points}
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking team changes.

  ## Examples

      iex> change_team(team)
      %Ecto.Changeset{data: %Team{}}

  """
  def change_team(%Team{} = team, attrs \\ %{}) do
    Team.changeset(team, attrs)
  end
end
