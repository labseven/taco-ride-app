defmodule TacoRide.Places do
  @moduledoc """
  The Places context.
  """

  import Ecto.Query, warn: false
  alias TacoRide.Repo

  alias TacoRide.Places.TacoShop

  @doc """
  Returns the list of taco_shops.

  ## Examples

      iex> list_taco_shops()
      [%TacoShop{}, ...]

  """
  def list_taco_shops do
    Repo.all(TacoShop)
    |> Repo.preload(reviews: [user: :team])
  end

  @doc """
  Gets a single taco_shop.

  Raises `Ecto.NoResultsError` if the Taco shop does not exist.

  ## Examples

      iex> get_taco_shop!(123)
      %TacoShop{}

      iex> get_taco_shop!(456)
      ** (Ecto.NoResultsError)

  """
  def get_taco_shop!(id), do: Repo.get!(TacoShop, id) |> Repo.preload(reviews: [user: :team])

  @doc """
  Creates a taco_shop.

  ## Examples

      iex> create_taco_shop(%{field: value})
      {:ok, %TacoShop{}}

      iex> create_taco_shop(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_taco_shop(attrs \\ %{}) do
    %TacoShop{}
    |> TacoShop.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a taco_shop.

  ## Examples

      iex> update_taco_shop(taco_shop, %{field: new_value})
      {:ok, %TacoShop{}}

      iex> update_taco_shop(taco_shop, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_taco_shop(%TacoShop{} = taco_shop, attrs) do
    taco_shop
    |> TacoShop.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a taco_shop.

  ## Examples

      iex> delete_taco_shop(taco_shop)
      {:ok, %TacoShop{}}

      iex> delete_taco_shop(taco_shop)
      {:error, %Ecto.Changeset{}}

  """
  def delete_taco_shop(%TacoShop{} = taco_shop) do
    Repo.delete(taco_shop)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking taco_shop changes.

  ## Examples

      iex> change_taco_shop(taco_shop)
      %Ecto.Changeset{data: %TacoShop{}}

  """
  def change_taco_shop(%TacoShop{} = taco_shop, attrs \\ %{}) do
    TacoShop.changeset(taco_shop, attrs)
  end
end
