defmodule TacoRide.Visits.Review do
  use Ecto.Schema
  import Ecto.Changeset

  schema "review" do
    field(:bike_rating, :integer)
    field(:taco_rating, :integer)

    field(:comment, :string)
    field(:photo_url, :string)
    field(:ate_all_tacos, :boolean, default: false)

    belongs_to(:taco_shop, TacoRide.Places.TacoShop)
    belongs_to(:user, TacoRide.Accounts.User)

    timestamps()
  end

  @doc false
  def changeset(review, attrs) do
    review
    |> cast(attrs, [
      :taco_rating,
      :bike_rating,
      :taco_shop_id,
      :user_id,
      :comment,
      :photo_url,
      :ate_all_tacos
    ])
    |> validate_required([:taco_rating, :bike_rating, :taco_shop_id, :user_id])
  end
end
