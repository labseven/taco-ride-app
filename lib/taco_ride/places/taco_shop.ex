defmodule TacoRide.Places.TacoShop do
  use Ecto.Schema
  import Ecto.Changeset

  schema "taco_shops" do
    field :address, :string
    field :name, :string
    field :place_id, :string

    field :location, Geo.PostGIS.Geometry

    has_many :reviews, TacoRide.Visits.Review

    timestamps()
  end

  @doc false
  def changeset(taco_shop, attrs) do
    taco_shop
    |> cast(attrs, [:name, :address, :place_id, :location])
    |> validate_required([:name, :location])
  end
end
